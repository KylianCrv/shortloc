<?php


namespace Cci\Core\Block;

use Magento\Catalog\Block\Product\ListProduct;

class Custom extends ListProduct
{
    public function getProducts()
    {
        $this->getLoadedProductCollection();
        return 'Liste des produits : ' . $this->getQtyProduct();
    }
}
